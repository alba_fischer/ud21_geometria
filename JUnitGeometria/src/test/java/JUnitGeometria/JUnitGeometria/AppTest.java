package JUnitGeometria.JUnitGeometria;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import JUnitGeometria.dto.Geometria;

class AppTest {

	@Test
	void test() {
	}
	//Area Cuadrado
	private static Stream<Arguments> getAreaCuadrado(){
		return Stream.of(
				Arguments.of(3, 9),
				Arguments.of(5, 25),
				Arguments.of(4, 16));
		}
		
	//Metodo test Area Cuadrado
	@ParameterizedTest
	@MethodSource ("getAreaCuadrado")
	public void testAreaCuadrado (int a, int b) {
		Geometria geo = new Geometria();
		int result = geo.areacuadrado(a);
		assertEquals(b,result);
	}
	//Area Circulo
	private static Stream<Arguments> getAreaCirculo(){
		return Stream.of(
				Arguments.of(5, 78.54),
				Arguments.of(3, 28.2744),
				Arguments.of(8, 201.0624));
		}
			
	//Metodo test Area Circulo
	@ParameterizedTest
	@MethodSource ("getAreaCirculo")
	public void testAreaCirculo (int a, double b) {
		Geometria geo = new Geometria();
		double result = geo.areaCirculo(a);
		assertEquals(b,result);
		}
	
	//Area Triangulo
	private static Stream<Arguments> getAreaTriangulo(){
		return Stream.of(
				Arguments.of(6,5,15),
				Arguments.of(2,3,3),
				Arguments.of(3,4,6));
			}
				
	//Metodo test Area Triangulo
	@ParameterizedTest
	@MethodSource ("getAreaTriangulo")
	public void testAreaTriangulo (int a, int b, int c) {
		Geometria geo = new Geometria();
		int result = geo.areatriangulo(a, b);
		assertEquals(c,result);
		}
	
	//Area Rectangulo
	private static Stream<Arguments> getAreaRectangulo(){
		return Stream.of(
				Arguments.of(6,5,30),
				Arguments.of(2,3,6),
				Arguments.of(3,4,12));
			}
					
	//Metodo test Area Rectangulo
	@ParameterizedTest
	@MethodSource ("getAreaRectangulo")
	public void testAreaRectangulo (int a, int b, int c) {
		Geometria geo = new Geometria();
		int result = geo.arearectangulo(a, b);
		assertEquals(c,result);
		}
	
	//Area Pentagono
	private static Stream<Arguments> getAreaPentagono(){
		return Stream.of(
				Arguments.of(6,5,15),
				Arguments.of(2,3,3),
				Arguments.of(3,4,6));
			}
					
	//Metodo test Area Pentagono
	@ParameterizedTest
	@MethodSource ("getAreaPentagono")
	public void testAreaPentagono (int a, int b, int c) {
		Geometria geo = new Geometria();
		int result = geo.areapentagono(a, b);
		assertEquals(c,result);
		}
	
	//Area Rombo
	private static Stream<Arguments> getAreaRombo(){
		return Stream.of(
				Arguments.of(6,5,15),
				Arguments.of(2,3,3),
				Arguments.of(3,4,6));
		}
						
	//Metodo test Area Rombo
	@ParameterizedTest
	@MethodSource ("getAreaRombo")
	public void testAreaRombo (int a, int b, int c) {
		Geometria geo = new Geometria();
		int result = geo.arearombo(a, b);
		assertEquals(c,result);
		}
	
	//Area Romboide
	private static Stream<Arguments> getAreaRomboide(){
		return Stream.of(
				Arguments.of(6,5,30),
				Arguments.of(2,3,6),
				Arguments.of(3,4,12));
		}
							
	//Metodo test Area Romboide
	@ParameterizedTest
	@MethodSource ("getAreaRomboide")
	public void testAreaRomboide (int a, int b, int c) {
		Geometria geo = new Geometria();
		int result = geo.arearomboide(a, b);
		assertEquals(c,result);
		}
	
	//Area Trapecio
	private static Stream<Arguments> getAreaTrapecio(){
		return Stream.of(
				Arguments.of(6,2,2,8),
				Arguments.of(2,4,6,18),
				Arguments.of(4,6,3,15));
		}
							
	//Metodo test Area Trapecio
	@ParameterizedTest
	@MethodSource ("getAreaTrapecio")
	public void testAreaTrapecio (int a, int b, int c, int d) {
		Geometria geo = new Geometria();
		int result = geo.areatrapecio(a, b, c);
		assertEquals(d,result);
		}
	
	//Getters1 Setters1
		private static Stream<Arguments> getGettersSetters1(){
			return Stream.of(
					Arguments.of(1,"cuadrado"),
					Arguments.of(2,"Circulo"),
					Arguments.of(3,"Triangulo"),
					Arguments.of(4,"Rectangulo"),
					Arguments.of(5,"Pentagono"),
					Arguments.of(6,"Rombo"),
					Arguments.of(7,"Romboide"),
					Arguments.of(8,"Trapecio"));
		}
		
		//Metodo test getters1 i setters1
		@ParameterizedTest
		@MethodSource ("getGettersSetters1")
		public void testGettersSetters1(int a, String b) {
			Geometria geo = new Geometria(a);
	    	geo.setId(a);
	    	geo.setNom(b);
	    	int id = geo.getId();
		   	String nom = geo.getNom(), figura = geo.figura(9);
	    	assertEquals(a,id);
	    	assertEquals(b,nom);
	    	assertEquals("Default",figura);
		}

	//Getters2 Setters2
	private static Stream<Arguments> getGettersSetters2(){
		return Stream.of(
				Arguments.of(1,23.6,"cuadrado","Geometria [id=" + 1 + ", nom=" + "cuadrado" + ", area=" + 23.6 + "]"),
				Arguments.of(2,3.4,"Circulo","Geometria [id=" + 2 + ", nom=" + "Circulo" + ", area=" + 3.4 + "]"),
				Arguments.of(3,10.5,"Triangulo","Geometria [id=" + 3 + ", nom=" + "Triangulo" + ", area=" + 10.5 + "]"),
				Arguments.of(4,9.7,"Rectangulo","Geometria [id=" + 4 + ", nom=" + "Rectangulo" + ", area=" + 9.7 + "]"),
				Arguments.of(5,8.0,"Pentagono","Geometria [id=" + 5 + ", nom=" + "Pentagono" + ", area=" + 8.0 + "]"),
				Arguments.of(6,17.8,"Rombo","Geometria [id=" + 6 + ", nom=" + "Rombo" + ", area=" + 17.8 + "]"),
				Arguments.of(7,3.2,"Romboide","Geometria [id=" + 7 + ", nom=" + "Romboide" + ", area=" + 3.2 + "]"),
				Arguments.of(8,20.0,"Trapecio","Geometria [id=" + 8 + ", nom=" + "Trapecio" + ", area=" + 20.0 + "]"));			
	}
	
	
	//Metodo test getters2 i setters2
	@ParameterizedTest
	@MethodSource ("getGettersSetters2")
	public void testGettersSetters2(int a, double b, String c, String d) {
    	Geometria geo = new Geometria(a);
    	int id = geo.getId();
    	geo.setArea(b);
    	double area = geo.getArea();
    	String string = geo.getNom(), toString = geo.toString();
    	assertEquals(a,id);
    	assertEquals(b,area);
    	assertEquals(c,string);
    	assertEquals(d,toString);
    }
	
}
